package com.ehrc.user.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ehrc.authentication.Secured;
import com.ehrc.db.UserDb;
import com.ehrc.user.co.UserCO;
import com.ehrc.user.co.UserOrgCO;
import com.ehrc.user.dao.FormDataDAOImpl;
import com.ehrc.user.dao.UserDAOImpl;
import com.ehrc.utility.EmailUtility;
import com.ehrc.utility.Encryptions;
import com.ehrc.utility.LoadConfig;
import com.ehrc.utility.ResponseConfig;

public class FormData {

	Logger logger = LoggerFactory.getLogger(FormData.class);
	FormDataDAOImpl objDAOImpl;

	
	@SuppressWarnings("unused")
	@POST
	@Path("/saveForm")
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response CreateUser(com.ehrc.db.FormData formObj ) throws Exception
	{
		logger.info("Entering method save Form ");
		String result = "Data saved";
		String strMessage="";
		com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
		responses.setLink("http://iiitb.ac.in/");
		responses.setLocation("save data");
		Response.Status objRespStat = null;
		
			if(formObj != null)
			{
					
				objDAOImpl.saveFormData(formObj);					
				return Response.status(Response.Status.ACCEPTED).entity(result).build();
				
			} else {
				strMessage="No data available";
				responses.setMessage(strMessage);
				objRespStat = Response.Status.BAD_REQUEST;
				responses.setStatus(objRespStat.getStatusCode());	
				return Response.status(objRespStat).entity(responses).build();
			}
			
	}
}
