package com.ehrc.user.rest;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ehrc.authentication.Secured;
import com.ehrc.db.UserDb;
import com.ehrc.db.UserOrgDb;
import com.ehrc.db.UserSessionDb;
import com.ehrc.user.co.UserCO;
import com.ehrc.user.co.UserOrgCO;
import com.ehrc.user.dao.UserDAOImpl;
import com.ehrc.user.dao.UserSessionDAOImpl;
import com.ehrc.utility.EmailUtility;
import com.ehrc.utility.Encryptions;
import com.ehrc.utility.JWTUtil;
import com.ehrc.utility.LoadConfig;
import com.ehrc.utility.Page;
import com.ehrc.utility.ResponseConfig;
import com.ehrc.user.co.UpdatePasswordCO;

@Path("/user")
public class User {
	
	Logger logger = LoggerFactory.getLogger(User.class);
	UserDAOImpl objUserDAOImpl;
	UserSessionDAOImpl objUserSesDAOImpl;
	String saltValue = LoadConfig.getConfigValue("SALT_KEY");
	
	public User()
	{
		objUserDAOImpl = new UserDAOImpl();
		objUserSesDAOImpl = new UserSessionDAOImpl();
		
	}
	
	@SuppressWarnings("unused")
	@GET
	@Path("/create")
//	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String CreateUser() throws Exception
	{
		return "Welcome";
	}


	@SuppressWarnings("unused")
	@POST
	@Path("/create")
//	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response CreateUser(UserCO objUserCO ) throws Exception
	{
		logger.info("Entering method with "+ objUserCO.getUsername());
		UserCO result = null;
		String strMessage="";
		com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
		responses.setLink("http://iiitb.ac.in/");
		responses.setLocation("create user");
		Response.Status objRespStat = null;
		
		Response objCheckUserResponse = new User().checkUsername(objUserCO.getUsername());
		if(objCheckUserResponse.getStatus() == 200) {
			if(objUserCO != null)
			{
				objUserCO.setStatus(LoadConfig.getConfigValue("ACTIVE_STATUS"));
				
				String decryptedPassString = Encryptions.decrypt(objUserCO.getPassword());
				//Storing the Decrypted password.
				objUserCO.setPassword(decryptedPassString);
//				objUserCO.setPassword(objUserCO.getPassword());
				
				result = objUserDAOImpl.saveUser(objUserCO);
				logger.info("Result received from DAO---- "+ result );
				if(result.getUsername() != null)
				{
					UserDb objUserDb = objUserDAOImpl.getUser(result.getUsername());
					
					logger.info("user ID from DB---- "+ objUserDb.getUserId());
					
					StringBuilder objBody = new StringBuilder();
					objBody.append(LoadConfig.getConfigValue("EMAIL_BODY_LINE1"));
					objBody.append("\nFirst Name : "+result.getFirstName());
					objBody.append("\nLast Name : "+result.getLastName());
					objBody.append("\nUsername : "+result.getUsername());
					objBody.append("\nPassword : "+result.getPassword());
					objBody.append("\nMobile No. : "+result.getMobileNo());
					objBody.append("\nEmail : "+result.getEmail());
					objBody.append("\nRole : "+result.getRole());
					objBody.append(LoadConfig.getConfigValue("EMAIL_NOTICE"));
					objBody.append(LoadConfig.getConfigValue("EMAIL_SALUTATION"));
					
					String emailBody = objBody.toString();
					
					
					result.setPassword(null);
					return Response.status(Response.Status.ACCEPTED).entity(result).build();
				}
				else
				{
					strMessage="Record could not be added";
					responses.setMessage(strMessage);
					objRespStat = Response.Status.UNAUTHORIZED;
					responses.setStatus(objRespStat.getStatusCode());	
					return Response.status(objRespStat).entity(responses).build();

				}
			} else {
				strMessage="No data available to create user";
				responses.setMessage(strMessage);
				objRespStat = Response.Status.BAD_REQUEST;
				responses.setStatus(objRespStat.getStatusCode());	
				return Response.status(objRespStat).entity(responses).build();
			}
		} else {
			logger.info("Username already taken.");
			responses.setMessage("Username already taken.");
			responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());	
			return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
		}		
	}
		
	
	@SuppressWarnings("unused")
	@POST
	@Path("/createAdminUser")
//	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response CreateAdmin(UserCO objUserCO ) throws Exception
	{
		logger.info("Entering method with "+ objUserCO.getUsername());
		UserCO result = null;
		String strMessage="";
		com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
		responses.setLink("http://iiitb.ac.in/");
		responses.setLocation("create user");
		Response.Status objRespStat = null;
		
		Response objCheckUserResponse = new User().checkUsername(objUserCO.getUsername());
		if(objCheckUserResponse.getStatus() == 200) {
			if(objUserCO != null)
			{
				objUserCO.setStatus(LoadConfig.getConfigValue("ACTIVE_STATUS"));
				
				String decryptedPassString = Encryptions.decrypt(objUserCO.getPassword());
				//Storing the Decrypted password.
				objUserCO.setPassword(decryptedPassString);
//				objUserCO.setPassword(objUserCO.getPassword());
				
				result = objUserDAOImpl.saveUser(objUserCO);
				logger.info("Result received from DAO---- "+ result );
				if(result.getUsername() != null)
				{
					UserDb objUserDb = objUserDAOImpl.getUser(result.getUsername());
					
					logger.info("user ID from DB---- "+ objUserDb.getUserId());
					StringBuilder objBody = new StringBuilder();
//					objBody.append("Hi Admin,\n");
					objBody.append("User created successfully. Below are the details for the user:\n");
					objBody.append("\nFirst Name : "+result.getFirstName());
					objBody.append("\nLast Name : "+result.getLastName());
					objBody.append("\nUsername : "+result.getUsername());
					objBody.append("\nPassword : "+result.getPassword());
					objBody.append("\nMobile No. : "+result.getMobileNo());
					objBody.append("\nEmail : "+result.getEmail());
					objBody.append("\nRole : "+result.getRole());
					objBody.append("\n\nThis is a system generated email, do not revert back.\n");
					objBody.append("\nRegards,\n");
					objBody.append("Migrants Portal Admin\n");
					
					String emailBody = objBody.toString();
					
					
					EmailUtility.sendRegisterServiceEmail(LoadConfig.getConfigValue("FROM_EMAIL_ID"), "User Created Successfully for KMHMS COVID BMR Portal", emailBody);
					result.setPassword(null);
					return Response.status(Response.Status.ACCEPTED).entity(result).build();
				}
				else
				{
					strMessage="Record could not be added";
					responses.setMessage(strMessage);
					objRespStat = Response.Status.UNAUTHORIZED;
					responses.setStatus(objRespStat.getStatusCode());	
					return Response.status(objRespStat).entity(responses).build();

				}
			} else {
				strMessage="No data available to create user";
				responses.setMessage(strMessage);
				objRespStat = Response.Status.BAD_REQUEST;
				responses.setStatus(objRespStat.getStatusCode());	
				return Response.status(objRespStat).entity(responses).build();
			}
		} else {
			logger.info("Username already taken.");
			responses.setMessage("Username already taken.");
			responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());	
			return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
		}		
	}
	
	
	@SuppressWarnings("static-access")
	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response userLogin(UserCO objUserCO) throws Exception {
		
		String username = objUserCO.getUsername();
		String password = objUserCO.getPassword();
		
		String decryptedPassString = Encryptions.decrypt(password);
		
		UserDb objUserDb = objUserDAOImpl.getUser(username);
		UserCO objUser = new UserCO();
		objUser.setPassword(objUserDb.getPassword());
		objUser = objUserDb.mapUserLoginDBToCO(objUserDb);
		
		if(objUser.getStatus().equals(LoadConfig.getConfigValue("ACTIVE_STATUS")) && objUser.getPassword().equals(decryptedPassString)) {
			objUser.setPassword(null);
			logger.info("Valid user");
			return Response.status(Response.Status.ACCEPTED).entity(objUser).build();
		} else {
			logger.info("Invalid user");
			com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
			responses.setLink("http://iiitb.ac.in/");
			responses.setLocation("login");
			responses.setMessage("Login Failed");
			responses.setStatus(Response.Status.UNAUTHORIZED.getStatusCode());	
			return Response.status(Response.Status.UNAUTHORIZED).entity(responses).build();
		}
	}
	
	@SuppressWarnings("static-access")
	@POST
	@Path("/loginj")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response userJWTLogin(UserCO objUserCO) throws Exception {
		
		String username = objUserCO.getUsername();
		String password = objUserCO.getPassword();

		String decryptedPassString = Encryptions.decrypt(password);
		
		UserDb objUserDb = objUserDAOImpl.getUser(username);
		UserCO objUser = new UserCO();
		objUser.setPassword(objUserDb.getPassword());
		objUser = objUserDb.mapUserLoginDBToCO(objUserDb);
		
		if(objUser.getStatus().equals(LoadConfig.getConfigValue("ACTIVE_STATUS")) && objUser.getPassword().equals(decryptedPassString)) {
			objUser.setPassword(null);
			
			
			String fetchedToken = getToken();
			logger.info("Token generated for user ->"+objUser.getUsername()+" as "+fetchedToken);
			long sesCount = objUserSesDAOImpl.getSession(fetchedToken);
									
			if(sesCount == 0) {
				logger.info("Unique Token found for user ->"+username+" as "+fetchedToken);
				UserSessionDb objUserSes = objUserSesDAOImpl.saveSession(fetchedToken, objUser.getUserId());
				if(objUserSes != null) {
					logger.info("Valid user");
					logger.info("User value = "+objUserSes.toString());
					String jwtToken = new JWTUtil().createUserToken(Integer.toString(objUserSes.getId()), objUserSes.getSessionToken(), objUser); 
					String token = "{\"token\":\""+jwtToken+"\"}";
					logger.info("Token Recieved = "+token);
					return Response.status(Response.Status.ACCEPTED).entity(token).build();
				}else {
					logger.info("Unable to create a session for the user. Login failed");
					com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
					responses.setLink("http://iiitb.ac.in/");
					responses.setLocation("login");
					responses.setMessage("Unable to create a session for the user. Login Failed");
					responses.setStatus(Response.Status.UNAUTHORIZED.getStatusCode());	
					return Response.status(Response.Status.UNAUTHORIZED).entity(responses).build();
				}
			} else {
				logger.info("Duplicate token found for user ->"+objUser.getUsername()+" as "+fetchedToken);
				logger.info("Invalid user");
				com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
				responses.setLink("http://iiitb.ac.in/");
				responses.setLocation("login");
				responses.setMessage("Session Already active");
				responses.setStatus(Response.Status.UNAUTHORIZED.getStatusCode());	
				return Response.status(Response.Status.UNAUTHORIZED).entity(responses).build();
			}
			
		} else {
			logger.info("Invalid user");
			com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
			responses.setLink("http://iiitb.ac.in/");
			responses.setLocation("login");
			responses.setMessage("Invalid Username/ Password");
			responses.setStatus(Response.Status.UNAUTHORIZED.getStatusCode());	
			return Response.status(Response.Status.UNAUTHORIZED).entity(responses).build();
		}
	}
	
	public String getToken() {
		String sesToken = UUID.randomUUID().toString();
		String localtime = Long.toString(System.currentTimeMillis());
		String timerToken = sesToken+localtime;
		String finalToken = UUID.nameUUIDFromBytes(timerToken.getBytes()).toString();
		return finalToken;
	}
	
	
	@SuppressWarnings({ "static-access", "unused" })
	@GET
	@Path("/loginj/refresh/{sesId}/{sesToken}/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response userJWTRefresh(@PathParam("sesId") String sesId, @PathParam("sesToken") String sesToken, @PathParam("username") String username) throws ParseException, IllegalArgumentException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException, IOException {
		logger.info("refreshing JWT for  = "+sesId);
		
		UserDb objUserDb = objUserDAOImpl.getUser(username);
		UserCO objUser = objUserDb.mapUserDBToCO(objUserDb);
		UserSessionDb objUserSesDB = objUserSesDAOImpl.getSession(sesId, sesToken);
		logger.info("Date diff =>>>>>>>>>>>>>>>>>>>> " + objUserSesDB.getExpiryAt().before(new Date()));
		if(objUserSesDB.getExpiryAt().after(new Date()) && objUserDb.getStatus().equals(LoadConfig.getConfigValue("ACTIVE_STATUS"))) {
			if (objUserSesDAOImpl.updateUserSession(sesId, objUser.getUserId(), sesToken)) {
				String jwtToken = new JWTUtil().createUserToken(sesId, sesToken, objUser);
				String token = "{\"token\":\"" + jwtToken + "\"}";
				logger.info("Token Recieved = " + token);
				return Response.status(Response.Status.ACCEPTED).entity(token).build();
			} else {
				Response res = logout(objUser.getUserId(), sesToken);
				logger.info("Unable to update the session for the user. Logging out.");
				com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
				responses.setLink("http://iiitb.ac.in/");
				responses.setLocation("refresh token");
				responses.setMessage("Unable to update the session for the user. Logging out.");
				responses.setStatus(Response.Status.FORBIDDEN.getStatusCode());	
				return Response.status(Response.Status.FORBIDDEN).entity(responses).build();
			}
		} else {
			Response res = logout(objUser.getUserId(), sesToken);
			logger.info("Session already expired. Please login again.");
			com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
			responses.setLink("http://iiitb.ac.in/");
			responses.setLocation("refresh token");
			responses.setMessage("Session already expired. Please login again.");
			responses.setStatus(Response.Status.FORBIDDEN.getStatusCode());	
			return Response.status(Response.Status.FORBIDDEN).entity(responses).build();
		}
	}
	
	@GET
	@Path("/logout/{userId}/{sessionToken}")
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response logout(@PathParam("userId") String userId, @PathParam("sessionToken") String sessionToken) throws ParseException {
		
		boolean result = new UserSessionDAOImpl().updateSession(userId, sessionToken);
		
		if(result != false ) {
			logger.info("Logged out Successfully.");
			com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
			responses.setLink("http://iiitb.ac.in/");
			responses.setLocation("logout");
			responses.setMessage("User logged out successfully");
			responses.setStatus(Response.Status.OK.getStatusCode());	
			return Response.status(Response.Status.OK).entity(responses).build();
		} else {
			logger.info("Logout failed");
			com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
			responses.setLink("http://iiitb.ac.in/");
			responses.setLocation("getUserByOrg");
			responses.setMessage("Logout failed");
			responses.setStatus(Response.Status.UNAUTHORIZED.getStatusCode());	
			return Response.status(Response.Status.UNAUTHORIZED).entity(responses).build();
		}
		
	}
	
	@SuppressWarnings("static-access")
	@GET
	@Path("/getUserByOrg/{orgId}")
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getUserByOrg(@PathParam("orgId") String orgId) throws ParseException {
		
		logger.info("orgId = "+orgId);
		List<UserOrgDb> objUserOrgDbList = objUserDAOImpl.getUserOrg(Integer.parseInt(orgId));
		UserDb objUserDb = null;
		List<UserCO> objUserCO = new ArrayList<UserCO>();
		if(objUserOrgDbList.size() != 0) {
			
			logger.info("List Size = "+objUserOrgDbList.size());
			for(UserOrgDb objUserorg : objUserOrgDbList) {
				objUserDb = new UserDb();
				objUserDb = objUserDAOImpl.getUserById(objUserorg.getUser_id());
				objUserDb.setPassword(null);
				objUserCO.add(objUserDb.mapUserDBToCO(objUserDb));
			}
			
			return Response.status(Response.Status.ACCEPTED).entity(objUserCO).build();
		} else {
			logger.info("Invalid user");
			com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
			responses.setLink("http://iiitb.ac.in/");
			responses.setLocation("getUserByOrg");
			responses.setMessage("Get User By Organization Failed");
			responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());	
			return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
		}		
	}

		

	@GET
	@Path("/check/{username}")
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response checkUsername(@PathParam("username") String username) throws ParseException {
		
		UserDb objUser = objUserDAOImpl.getUser(username);
		if(objUser != null) {
			logger.info("Username already taken.");
			com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
			responses.setLink("http://iiitb.ac.in/");
			responses.setLocation("check");
			responses.setMessage("Username already taken");
			responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());	
			return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
		} else {
			logger.info("Username available for use.");
			com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
			responses.setLink("http://iiitb.ac.in/");
			responses.setLocation("check");
			responses.setMessage("Username available for use");
			responses.setStatus(Response.Status.OK.getStatusCode());	
			return Response.status(Response.Status.OK).entity(responses).build();
		}
	}
	
	@SuppressWarnings("static-access")
	@GET
	@Path("/secure/All/password")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response checkUsername() throws ParseException {
		List<UserDb> objUser = objUserDAOImpl.getAllUserPass();
		UserCO objUserCO = null;
//		String saltValue = genSalt();
//		String saltValue = LoadConfig.getConfigValue("SALT_KEY");
		logger.info("Salt value = "+saltValue);
		
		String[] userId = {new String()};
		if(objUser != null) {
			int i=0;
			for(UserDb userPass : objUser) {
				objUserCO = new UserCO();
				objUserCO = userPass.mapUserLoginDBToCO(userPass);
				String finalPassword = BCrypt.hashpw(objUserCO.getPassword(), saltValue);
				logger.info(i+". Original Password = "+objUserCO.getPassword());
				logger.info(i+". Hashed Password = "+finalPassword);
				objUserCO.setPassword(finalPassword);
				
				if(!objUserDAOImpl.updatePassword(objUserCO)) {
					userId[i] = objUserCO.getUserId();
					i++;
				}
				
			}
			
			if(userId.length == 0) {
				logger.info("Password secured for all.");
				com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
				responses.setLink("http://iiitb.ac.in/");
				responses.setLocation("secure password");
				responses.setMessage("Password secured for all.");
				responses.setStatus(Response.Status.ACCEPTED.getStatusCode());	
				return Response.status(Response.Status.ACCEPTED).entity(responses).build();
			} else {
				logger.info("Unable to secure Passwords for all.");
				com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
				responses.setLink("http://iiitb.ac.in/");
				responses.setLocation("secure password");
				responses.setMessage("Unable to secure Passwords for all. Please find the list of failed users "+userId);
				responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());	
				return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
			}
			
		} else {
			logger.info("No data to update");
			com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
			responses.setLink("http://iiitb.ac.in/");
			responses.setLocation("check");
			responses.setMessage("no data to update.");
			responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());	
			return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
		}
	}
	
	public static String genSalt() {

		return BCrypt.gensalt();
	}
	
	
	@SuppressWarnings({ "static-access" })
	@POST
	@Path("/update")
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateUser(UserCO objUserCo) throws ParseException {
		
		int userId = Integer.parseInt(objUserCo.getUserId());
		String status = new UserDAOImpl().getStatus(userId);
		
		UserDb objUserDb = new UserDb().mapUserUpdateCOToDB(objUserCo);
		
		if(status == null || status.equals("null")) {
			logger.info("Invalid user");
			com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
			responses.setLink("http://iiitb.ac.in/");
			responses.setLocation("updateUser");
			responses.setMessage("{ \"status\":\"Update User failed.\"}");
			responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());	
			return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
		} else {
			if(status.equals(LoadConfig.getConfigValue("ACTIVE_STATUS"))) {
				
				
				
				if(new UserDAOImpl().updateUser(objUserDb)) {
					return Response.status(Response.Status.OK).entity("{ \"status\":\"User updated successfully.\"}").build();
				} else {
					logger.info("Invalid user");
					com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
					responses.setLink("http://iiitb.ac.in/");
					responses.setLocation("updateUser");
					responses.setMessage("{ \"status\":\"Update User failed.\"}");
					responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());	
					return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
				}
			} else if(status.equals(LoadConfig.getConfigValue("INACTIVE_STATUS"))) {
				if(new UserDAOImpl().updateUser(objUserDb)) {
					return Response.status(Response.Status.OK).entity("{ \"status\":\"User updated successfully.\"}").build();
				} else {
					logger.info("Invalid user");
					com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
					responses.setLink("http://iiitb.ac.in/");
					responses.setLocation("updateUser");
					responses.setMessage("{ \"status\":\"Update User failed.\"}");
					responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());	
					return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
				}
			} else {
				logger.info("Invalid user");
				com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
				responses.setLink("http://iiitb.ac.in/");
				responses.setLocation("updateUser");
				responses.setMessage("{ \"status\":\"Update User failed.\"}");
				responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());	
				return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
			}
		}
		
				
	}
	
	@SuppressWarnings("static-access")
	@POST
	@Path("/updatePassword")
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updatePassword(UpdatePasswordCO objUpdatePass) throws ParseException {
		
		UserDb objUser = new UserDAOImpl().getUserById(objUpdatePass.getUserId());
		
		if(objUser.getStatus().equals(LoadConfig.getConfigValue("ACTIVE_STATUS"))) {
			
			
			try {
				String decryptedCurrentPass = Encryptions.decrypt(objUpdatePass.getCurrentPassword());
				String decryptedNewPass = Encryptions.decrypt(objUpdatePass.getNewPassword());				
				if(decryptedCurrentPass.equals(objUser.getPassword())) {
					if(new UserDAOImpl().updatePass(objUpdatePass.getUserId(), decryptedNewPass)) {
						logger.info("Password Updated Successfully.");
						com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
						responses.setLink("http://iiitb.ac.in/");
						responses.setLocation("updatePassword");
						responses.setMessage("{ \"status\":\"Password updated successfully.\"}");
						responses.setStatus(Response.Status.OK.getStatusCode());	
						return Response.status(Response.Status.OK).entity(responses).build();
					} else {
						logger.info("Current Password did not match the provided string.");
						com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
						responses.setLink("http://iiitb.ac.in/");
						responses.setLocation("updatePassword");
						responses.setMessage("{ \"status\":\"Something went wrong. Password updation failed. Please contact Administrator.\"}");
						responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());	
						return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
					}
				} else {
					logger.info("Current Password did not match the provided string.");
					com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
					responses.setLink("http://iiitb.ac.in/");
					responses.setLocation("updatePassword");
					responses.setMessage("{ \"status\":\"Current Password did not match the provided string.\"}");
					responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());	
					return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.info("Current Password did not match the provided string.");
				com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
				responses.setLink("http://iiitb.ac.in/");
				responses.setLocation("updatePassword");
				responses.setMessage("{ \"status\":\"Something went wrong. Password updation failed. Please contact Administrator.\"}");
				responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());	
				return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
			}
			
			
		} else  {
			logger.info("Invalid user");
			com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
			responses.setLink("http://iiitb.ac.in/");
			responses.setLocation("updatePassword");
			responses.setMessage("{ \"status\":\"Update Password Failed. Seems like the account has been deactivated.\"}");
			responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());	
			return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
		}		
	}

	
	@SuppressWarnings("static-access")
	@GET
	@Path("/getAllUser/paginate")
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getUserOrg(@QueryParam("pageNumber") int pageNumber, @QueryParam("pageSize") int pageSize) throws ParseException {
		
		logger.info("Getting all users = ");
		Page<UserCO> objUserCOPage = objUserDAOImpl.getAllUser(pageNumber, pageSize);
		if(objUserCOPage.getSize() != 0) {
			
			List<UserCO> objUserDbList = objUserCOPage.getContent();
			logger.info("List Size = "+objUserDbList.size());
			return Response.status(Response.Status.ACCEPTED).entity(objUserCOPage).build();
		} else {
			logger.info("Invalid user");
			com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
			responses.setLink("http://iiitb.ac.in/");
			responses.setLocation("getUserByOrg");
			responses.setMessage("Get User By Organization Failed");
			responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());	
			return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
		}		
	}
	
	
	public static void main(String[] args) {
		
	
	}
	
}
