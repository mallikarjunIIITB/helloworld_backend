package com.ehrc.reports.models;

import java.sql.Timestamp;
import java.util.UUID;

import org.dom4j.Text;

public class Reports {
	String call_id;
	String user_id;
	String call_type;
	String queue_language;
	Timestamp created_at;
	Text masked_caller_phone;
	Integer year_of_birth;
	UUID patient_id;
	String gender;
	String individual_calling;
	String district;
	String triage;
	String resolution;
	String complaints;
	Integer campaign_id;
	
	public String getCall_id() {
		return call_id;
	}
	public void setCall_id(String call_id) {
		this.call_id = call_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getCall_type() {
		return call_type;
	}
	public void setCall_type(String call_type) {
		this.call_type = call_type;
	}
	public String getQueue_language() {
		return queue_language;
	}
	public void setQueue_language(String queue_language) {
		this.queue_language = queue_language;
	}
	public Timestamp getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}
	public Text getMasked_caller_phone() {
		return masked_caller_phone;
	}
	public void setMasked_caller_phone(Text masked_caller_phone) {
		this.masked_caller_phone = masked_caller_phone;
	}
	public Integer getYear_of_birth() {
		return year_of_birth;
	}
	public void setYear_of_birth(Integer year_of_birth) {
		this.year_of_birth = year_of_birth;
	}
	public UUID getPatient_id() {
		return patient_id;
	}
	public void setPatient_id(UUID patient_id) {
		this.patient_id = patient_id;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getIndividual_calling() {
		return individual_calling;
	}
	public void setIndividual_calling(String individual_calling) {
		this.individual_calling = individual_calling;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getTriage() {
		return triage;
	}
	public void setTriage(String triage) {
		this.triage = triage;
	}
	public String getResolution() {
		return resolution;
	}
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	public String getComplaints() {
		return complaints;
	}
	public void setComplaints(String complaints) {
		this.complaints = complaints;
	}
	public Integer getCampaign_id() {
		return campaign_id;
	}
	public void setCampaign_id(Integer campaign_id) {
		this.campaign_id = campaign_id;
	}
	
	@Override
	public String toString() {
		return "Reports [call_id=" + call_id + ", user_id=" + user_id + ", call_type=" + call_type + ", queue_language="
				+ queue_language + ", created_at=" + created_at + ", masked_caller_phone=" + masked_caller_phone
				+ ", year_of_birth=" + year_of_birth + ", patient_id=" + patient_id + ", gender=" + gender
				+ ", individual_calling=" + individual_calling + ", district=" + district + ", triage=" + triage
				+ ", resolution=" + resolution + ", complaints=" + complaints + ", campaign_id=" + campaign_id + "]";
	}
	
	
	
	
}
