package com.ehrc.reports.db.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.annotations.common.util.impl.LoggerFactory;

import com.ehrc.reports.models.Reports;
import com.ehrc.utility.HibernateUtil;



public class ReportsDataDaoImpl implements ReportDAO {

		//static Logger logger = LoggerFactory.getLogger(reportsDataDaoImpl.class);
		
		public Reports[] getRecords(int campaign_id, String startDate, String endDate) {
			Reports[] arrPeopleCO = null;
		Session ses = HibernateUtil.getSession();
		try {
		
			TypedQuery<Reports> query = ses.createQuery(
			        "SELECT c.call_id, c.user_id, c.call_type, c.queue_language, p.masked_caller_phone, c.created_time, " +
			        "p.year_of_birth, p.patient_id, g.name as gender, i.name as individual_calling, d.name as district, " +
			        "cd.triage, cd.resolution, cd.all_complaints as complaints " +
			        "FROM call c " +
			        "JOIN patient p ON c.telemanas_id = p.telemanas_id " +
			        "LEFT JOIN config_gender g ON p.gender = g.id " +
			        "LEFT JOIN config_district d ON p.district_id = d.id " +
			        "LEFT JOIN config_individual_calling i ON c.individual_calling = i.id " +
			        "JOIN counselling_data cd ON cd.call_id = c.call_id " +
			        "WHERE c.campaign_id = :campaign_id " +
			        "AND DATE_TRUNC('day', c.created_time) >= :startDate " +
			        "AND DATE_TRUNC('day', c.created_time) <= :endDate",
			        Reports.class);

			query.setParameter("campaignid", campaign_id);
			query.setParameter("startDate", startDate);
			query.setParameter("endDate", endDate);

			List<Reports> results = query.getResultList();
		int lsize = results.size();
		if (lsize > 0) {
		arrPeopleCO = new Reports[lsize];
		for (int i = 0; i < lsize; i++) {
			Reports peopleDb = (Reports) results.get(i);
//		arrPeopleCO[i] = State_details.mapPeopleDBToCO(peopleDb);
//		arrPeopleCO[i].setAddress_stranded(prepareAddressCO(ses, State_details.getStranded_address_id()));
//		arrPeopleCO[i].setPerm_address(prepareAddressCO(ses, State_details.getPerm_address_id()));
//		arrPeopleCO[i].setBelong_district(PeopleDAOImpl.getPeopleBelongDist(ses, peopleDb));

		}
		}
		} catch (Exception e) {
		//logger.error("Problem fetcing people records", e);
		} finally {
		HibernateUtil.closeSession();
		}
		return arrPeopleCO;
		}

}
