package com.ehrc.reports.db.dao;

import com.ehrc.reports.models.Reports;

public interface ReportDAO {
	
//	public interface GenerateReportDAO {
		public Reports[] getRecords(int campaign_id, String startDate, String endDate);

//	}

}
