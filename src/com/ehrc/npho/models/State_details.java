package com.ehrc.npho.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import com.ehrc.utility.PatternUtil;



@Entity
@Table(name = "State_Details")
public class State_details {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	
	@Pattern(message = PatternUtil.patternMessage, regexp = PatternUtil.address)
	@Column(name = "state_name", length = 50)
	String state_name;
	
	@Pattern(message = PatternUtil.patternMessage, regexp = PatternUtil.phoneNumberOnly)
	@Column(name = "state_id")
	int state_id;
	
	@Pattern(message = PatternUtil.patternMessage, regexp = PatternUtil.phoneNumberOnly)
	@Column(name = "campaign_id")
	Integer campaign_id;
	
	@Pattern(message = PatternUtil.patternMessage, regexp = PatternUtil.phoneNumberOnly)
	@Column(name = "total_facilities_registered")
	Integer total_facilities_registered;
	
	@Pattern(message = PatternUtil.patternMessage, regexp = PatternUtil.phoneNumberOnly)
	@Column(name = "total_registered_ivrs_users")
	Integer total_registered_ivrs_users;
	
	
	@Pattern(message = PatternUtil.patternMessage, regexp = PatternUtil.phoneNumberOnly)
	@Column(name = "total_number_of_incoming_calls")
	Integer total_number_of_incoming_calls;
	
	@Pattern(message = PatternUtil.patternMessage, regexp = PatternUtil.phoneNumberOnly)
	@Column(name = "total_number_of_multiple_callers")
	Integer total_number_of_multiple_callers;
	
	@Pattern(message = PatternUtil.patternMessage, regexp = PatternUtil.phoneNumberOnly)
	@Column(name = "total_number_of_calls_back")
	Integer total_number_of_calls_back;
	
	@Pattern(message = PatternUtil.patternMessage, regexp = PatternUtil.phoneNumberOnly)
	@Column(name = "total_number_of_incoming_calls_today")
	Integer total_number_of_incoming_calls_today;
	
	@Pattern(message = PatternUtil.patternMessage, regexp = PatternUtil.address)
	@Column(name = "type_of_call" , length = 100)
	String type_of_call;
	
	@Pattern(message = PatternUtil.patternMessage, regexp = PatternUtil.address)
	@Column(name = "category_of_call",  length = 5000)
	String category_of_call;
	
	@Pattern(message = PatternUtil.patternMessage, regexp = PatternUtil.phoneNumberOnly)
	@Column(name = "number_of_female_callers")
	Integer number_of_female_callers;
	
	@Pattern(message = PatternUtil.patternMessage, regexp = PatternUtil.phoneNumberOnly)
	@Column(name = "number_of_male_callers")
	Integer number_of_male_callers;

	@Column(name = "total_ivrs_users_active_today")
	Integer total_ivrs_users_active_today;
	
	@Column(name = "number_of_calls_dropped")
	Integer number_of_calls_dropped;

	
	public Integer getNumber_of_calls_dropped() {
		return number_of_calls_dropped;
	}

	public void setNumber_of_calls_dropped(Integer number_of_calls_dropped) {
		this.number_of_calls_dropped = number_of_calls_dropped;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getState_name() {
		return state_name;
	}

	public void setState_name(String state_name) {
		this.state_name = state_name;
	}

	public int getState_id() {
		return state_id;
	}

	public void setState_id(int state_id) {
		this.state_id = state_id;
	}

	public Integer getCampaign_id() {
		return campaign_id;
	}

	public void setCampaign_id(Integer campaign_id) {
		this.campaign_id = campaign_id;
	}


	public String getType_of_call() {
		return type_of_call;
	}

	public void setType_of_call(String type_of_call) {
		this.type_of_call = type_of_call;
	}

	public String getCategory_of_call() {
		return category_of_call;
	}

	public void setCategory_of_call(String category_of_call) {
		this.category_of_call = category_of_call;
	}

	
	public Integer getTotal_facilities_registered() {
		return total_facilities_registered;
	}

	public void setTotal_facilities_registered(Integer total_facilities_registered) {
		this.total_facilities_registered = total_facilities_registered;
	}

	public Integer getTotal_registered_ivrs_users() {
		return total_registered_ivrs_users;
	}

	public void setTotal_registered_ivrs_users(Integer total_registered_ivrs_users) {
		this.total_registered_ivrs_users = total_registered_ivrs_users;
	}

	public Integer getTotal_number_of_incoming_calls() {
		return total_number_of_incoming_calls;
	}

	public void setTotal_number_of_incoming_calls(Integer total_number_of_incoming_calls) {
		this.total_number_of_incoming_calls = total_number_of_incoming_calls;
	}

	public Integer getTotal_number_of_multiple_callers() {
		return total_number_of_multiple_callers;
	}

	public void setTotal_number_of_multiple_callers(Integer total_number_of_multiple_callers) {
		this.total_number_of_multiple_callers = total_number_of_multiple_callers;
	}

	public Integer getTotal_number_of_calls_back() {
		return total_number_of_calls_back;
	}

	public void setTotal_number_of_calls_back(Integer total_number_of_calls_back) {
		this.total_number_of_calls_back = total_number_of_calls_back;
	}

	public Integer getTotal_number_of_incoming_calls_today() {
		return total_number_of_incoming_calls_today;
	}

	public void setTotal_number_of_incoming_calls_today(Integer total_number_of_incoming_calls_today) {
		this.total_number_of_incoming_calls_today = total_number_of_incoming_calls_today;
	}

	public Integer getNumber_of_female_callers() {
		return number_of_female_callers;
	}

	public void setNumber_of_female_callers(Integer number_of_female_callers) {
		this.number_of_female_callers = number_of_female_callers;
	}

	public Integer getNumber_of_male_callers() {
		return number_of_male_callers;
	}

	public void setNumber_of_male_callers(Integer number_of_male_callers) {
		this.number_of_male_callers = number_of_male_callers;
	}

	
	public Integer getTotal_ivrs_users_active_today() {
		return total_ivrs_users_active_today;
	}

	public void setTotal_ivrs_users_active_today(Integer total_ivrs_users_active_today) {
		this.total_ivrs_users_active_today = total_ivrs_users_active_today;
	}

	@Override
	public String toString() {
		return "State_details [id=" + id + ", state_name=" + state_name + ", state_id=" + state_id + ", campaign_id="
				+ campaign_id + ", total_facilities_registered=" + total_facilities_registered
				+ ", total_registered_ivrs_users=" + total_registered_ivrs_users + ", total_number_of_incoming_calls="
				+ total_number_of_incoming_calls + ", total_number_of_multiple_callers="
				+ total_number_of_multiple_callers + ", total_number_of_calls_back=" + total_number_of_calls_back
				+ ", total_number_of_incoming_calls_today=" + total_number_of_incoming_calls_today + ", type_of_call="
				+ type_of_call + ", category_of_call=" + category_of_call + ", number_of_female_callers="
				+ number_of_female_callers + ", number_of_male_callers=" + number_of_male_callers + "]";
	}

	

	
	
	
}
