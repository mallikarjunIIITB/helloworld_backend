package com.ehrc.npho.services;

import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ehrc.npho.db.dao.NphoDataDaoImpl;
import com.ehrc.npho.models.State_details;


@Path("/rest/v0")
public class nphoServices {
	
	Logger logger = LoggerFactory.getLogger(nphoServices.class);
//	String token = "Te1eman@$User"; //VGUxZW1hbkAkVXNlcg==  for DEV
	String token = "Te1ekpmg@$User"; //VGUxZWtwbWdAJFVzZXI=  for PROD

	
	// Get All call Counts based on state
		@SuppressWarnings("unused")
		@POST
		@Path("/getCallCountByState")
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
		public Response getRecord(State_details state, @Context HttpHeaders hr) throws Exception {
			Response objResponse = null;
			String auth = hr.getHeaderString("Authorization");
			
			NphoDataDaoImpl dao = new NphoDataDaoImpl();
			Response res = null;
			
			int state_id = state.getState_id();
			StringBuilder sb = new StringBuilder();
			try {
				String dec = Decode(auth);
				if(token.equals(dec)) {
				List<State_details> cd = dao.getCallData(state_id);
				if(cd != null) {
					logger.info("record of>>" + state_id);
							objResponse = Response.ok(cd, MediaType.APPLICATION_JSON).build();
				} else {
				     sb.append("{");
				     sb.append("\"message\" : \""+"Record Not Found"+"\"");
				     sb.append("}");
					objResponse = Response.status(Response.Status.NO_CONTENT).entity(sb.toString()).build();
				}
			} else {
				   sb.append("{");
				     sb.append("\"message\" : \""+"Unauthorized"+"\"");
				     sb.append("}");
					objResponse = Response.status(Response.Status.UNAUTHORIZED).entity(sb.toString()).build();
		
			}
			} catch (Exception e) {
				e.printStackTrace();
				sb.append("{");
				sb.append("\"message\" : \"" + "Unauthorized" + "\"");
				sb.append("}");
				objResponse = Response.status(Response.Status.UNAUTHORIZED).entity(sb.toString()).build();

			}
			return objResponse;
		}
		
		public void EncodeDecode(String s) {
		    //ENCODE
		    byte[] helloBytes = s.getBytes(StandardCharsets.UTF_8);
		    String encodedHello = DatatypeConverter.printBase64Binary(helloBytes);
		}
		
		public String Decode(String encodedHello) {
			 byte[] encodedHelloBytes = DatatypeConverter.parseBase64Binary(encodedHello);
			    String dec = new String(encodedHelloBytes, StandardCharsets.UTF_8) ;
			    return dec;
		}

}
