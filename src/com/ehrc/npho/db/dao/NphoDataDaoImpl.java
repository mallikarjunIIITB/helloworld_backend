package com.ehrc.npho.db.dao;

import java.util.List;

import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ehrc.npho.models.State_details;
import com.ehrc.utility.HibernateUtil;




public class NphoDataDaoImpl {
	
	static Logger logger = LoggerFactory.getLogger(NphoDataDaoImpl.class);

	@SuppressWarnings({ "unchecked", "null" })
	public List<State_details> getCallData(int state_id) {
		Session ses = null;
		List<State_details> InfoData = null;
		try {
			ses = HibernateUtil.getSession();
			TypedQuery<State_details> query = null;
			
			if(state_id == 0 ) {
			query = ses.createQuery("select map from State_details map");
			} else {
				query = ses.createQuery("Select map from State_details map where map.state_id = :state_id");
				query.setParameter("state_id", state_id);
			}
			List<State_details> results = query.getResultList();
//			 for (State_details r : results) {
//				 State_details sd = new State_details();
//
//				 sd.setId(r.getId());
//				 
//				 if (r.getState_name() != null) {
//				 sd.setState_name(r.getState_name());
//				 }else {
//					 sd.setState_name(null); 
//				 }
//					 
//				 sd.setState_id(r.getState_id());
//				 
//				 if (r.getCampaign_id() != 0) {
//				 sd.setCampaign_id(r.getCampaign_id());
//				 } else {
//					 sd.setCampaign_id(0);
//				 }
//				 
//				 if (r.getTotal_facilities_registered() != 0) {
//				 sd.setTotal_facilities_registered(r.getTotal_facilities_registered());
//				 }else {
//					 sd.setTotal_facilities_registered(0);
//				 }
//				 
//				 if (r.getTotal_registered_ivrs_users() != 0) {
//				 sd.setTotal_registered_ivrs_users(r.getTotal_registered_ivrs_users());
//				 } else {
//					 sd.setTotal_registered_ivrs_users(0);
//				 }
//				 
//				 if (r.getTotal_number_of_incoming_calls() != 0) {
//				 sd.setTotal_number_of_incoming_calls(r.getTotal_number_of_incoming_calls());
//				 } else {
//					 sd.setTotal_number_of_incoming_calls(0);
//				 }
//				
//				 if (r.getTotal_number_of_multiple_callers() != 0) {
//				 sd.setTotal_number_of_multiple_callers(r.getTotal_number_of_multiple_callers());
//				 } else {
//					 sd.setTotal_number_of_multiple_callers(0);
//				 }
//				 
//				 if (r.getTotal_number_of_calls_back() != 0) {
//				 sd.setTotal_number_of_calls_back(r.getTotal_number_of_calls_back());
//				 }else {
//					 sd.setTotal_number_of_calls_back(0); 
//				 }
//				 
//				 if (r.getTotal_number_of_incoming_calls_today() != 0) {
//				 sd.setTotal_number_of_incoming_calls_today(r.getTotal_number_of_incoming_calls_today());
//				 } else {
//					 sd.setTotal_number_of_incoming_calls_today(0);
//				 }
//				 
//				 if (r.getType_of_call() != null) {
//				 sd.setType_of_call(r.getType_of_call());
//				 } else { 
//					 sd.setType_of_call(null);
//				 }
//				 
//				 if (r.getCategory_of_call() != null) {
//				 sd.setCategory_of_call(r.getCategory_of_call());
//				 }else {
//					 sd.setCategory_of_call(null);
//				 }
//				 
//				 if (r.getNumber_of_female_callers() != 0) {
//				 sd.setNumber_of_female_callers(r.getNumber_of_female_callers());
//				 } else {
//					 sd.setNumber_of_female_callers(0);
//				 }
//				 
//				 if (r.getNumber_of_male_callers() != 0) {
//				 sd.setNumber_of_male_callers(r.getNumber_of_male_callers());
//				 } else {
//					 sd.setNumber_of_male_callers(0);
//				 }

//				 InfoData.add(sd);
//			 }
			 return results;
			
	    						
		}

		catch (NonUniqueResultException e) {
			logger.error("DB error for get record :- ", e);
			return null;
		} catch (Exception e) {
			logger.error("DB error in Max :- ", e);

		} finally {
			HibernateUtil.closeSession();
		}
		return InfoData;
	}
	
}
