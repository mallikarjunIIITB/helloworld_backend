package com.ehrc.report.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.dom4j.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ehrc.authentication.Secured;
import com.ehrc.npho.services.nphoServices;
import com.ehrc.reports.db.dao.ReportsDataDaoImpl;
import com.ehrc.reports.models.Reports;
import com.ehrc.utility.ResponseConfig;




@Path("/rest/v0")
public class reportService {
	
	Logger logger = LoggerFactory.getLogger(reportService.class);
	public String CONFIG_PATH = null;
	ReportsDataDaoImpl generate;
	
	
	@SuppressWarnings({ "unused", "null" })
	@GET
	@Path("/generateReport/{campaignid}/{startDate}/{endDate}")
	@Secured
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response fetchRecord(@PathParam("campaign_id") int campaign_id, @PathParam("startDate") String startDate,
	@PathParam("endDate") String endDate) {
//	DistrictDAOImpl objDistrictDAO = new DistrictDAOImpl();
//	StateDAOImpl objStateDAO = new StateDAOImpl();
		Reports[] c = generate.getRecords(campaign_id, startDate, endDate);
	PrintWriter pw = null;

	//CONFIG_PATH = System.getProperty("/etc");
	CONFIG_PATH = "/etc/config/report";
	com.ehrc.utility.ResponseConfig responses = new ResponseConfig();
	try {
	OutputStream os1 = new FileOutputStream(CONFIG_PATH + "/" + "NewData.csv");
	try {
	pw = new PrintWriter(new OutputStreamWriter(os1, "UTF-8"));
	} catch (UnsupportedEncodingException e) {
	e.printStackTrace();
	}
	} catch (FileNotFoundException e) {
	e.printStackTrace();
	}

	logger.info("Entering method - GenerateReports>> " );
	StringBuilder sb = new StringBuilder();

	sb.append("Sl No,");
	sb.append("Call_id,");
	sb.append("User_id,");
	sb.append("Call_type");
	sb.append("Queue_language");
	sb.append("Masked_caller_phone,");
	sb.append("Created_time,");
	sb.append("Year_of_birth");
	sb.append("Patient_id");
	sb.append("Gender,");
	sb.append("Individual_calling,");
	sb.append("District");
	sb.append("Triage");
	sb.append("Resolution,");
	sb.append("Complaints,");
	sb.append("Campaign_id");
	sb.append('\n');
	

	Reports[] arrPeopleCO = { new Reports() };
	
	
	arrPeopleCO = generate.getRecords(campaign_id, startDate, endDate);
	
	String Call_id = null;
	String User_id = null;
	String Call_type = null;
	String Queue_language = null;
	int Masked_caller_phone = 0;
	int Year_of_birth = 0;
	int Patient_id = 0;
	String Gender = null;
	String Individual_calling = null;
	String District = null;
	String Triage = null;
	String Resolution = null;
	String Complaints = null;
	int Campaign_id = 0;
	
	
	sb.append('\n');
	sb.append("Call_id,");
	sb.append(',');
	sb.append("User_id,");
	sb.append(',');
	sb.append("Call_type");
	sb.append(',');
	sb.append("Queue_language");
	sb.append(',');
	sb.append("Masked_caller_phone,");
	sb.append(',');
	sb.append("Created_time,");
	sb.append(',');
	sb.append("Year_of_birth");
	sb.append(',');
	sb.append("Patient_id");
	sb.append(',');
	sb.append("Gender,");
	sb.append(',');
	sb.append("Individual_calling,");
	sb.append(',');
	sb.append("District");
	sb.append(',');
	sb.append("Triage");
	sb.append(',');
	sb.append("Resolution,");
	sb.append(',');
	sb.append("Complaints,");
	sb.append(',');
	sb.append("Campaign_id");
	sb.append(',');
	sb.append('\n');
	pw.write(sb.toString());
	pw.close();
	
	try {
	String filePath = CONFIG_PATH + "/" + "NewData.csv";
	File file = new File(filePath);
	String file_name = "NewData.csv";
	logger.info("File name = " + file_name);
	ResponseBuilder response = Response.ok((Object) file);
	response.header("Content-Disposition", "attachment; filename=\"" + file_name + "\"");

	return response.build();
	} catch (Exception e) {
	logger.error("Error :- ", e);
	ResponseConfig responses1 = new ResponseConfig();
	responses.setCode(Response.Status.BAD_REQUEST.getStatusCode());
	responses.setLocation("download");
	responses.setMessage("Unable to download File. Something went wrong.");
	responses.setStatus(Response.Status.BAD_REQUEST.getStatusCode());
	return Response.status(Response.Status.BAD_REQUEST).entity(responses).build();
	}
	}

}
