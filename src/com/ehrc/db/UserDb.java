package com.ehrc.db;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnTransformer;

import com.ehrc.user.co.UserCO;

@Entity
@Table(name = "userdb")
public class UserDb {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int userId;
	
	@Column(name = "username", length = 50)
	String username;
	
	@ColumnTransformer(read = "pgp_sym_decrypt(password, 'abcd')", write = "pgp_sym_encrypt(?, 'abcd')")
	@Column(columnDefinition = "bytea", name="password")
	String password;
	
	@ColumnTransformer(read = "pgp_sym_decrypt(email, 'abcd')", write = "pgp_sym_encrypt(?, 'abcd')")
	@Column(columnDefinition = "bytea", name="email")
	String email;
	
	@Column(name = "first_name", length = 100)
	String first_name;
	
	@Column(name = "last_name", length = 100)
	String last_name;
	
	@ColumnTransformer(read = "pgp_sym_decrypt(mobile_no, 'abcd')", write = "pgp_sym_encrypt(?, 'abcd')")
	@Column(columnDefinition = "bytea", name="mobile_no")
	String mobile_no;
	
	@Column(name = "role", length = 100)
	String role;
	
	@Column(name = "designation", length = 100)
	String designation;
	
	@Column(name = "associatedOrg", length = 100)
	String associatedOrg;
	
	
	@Column(name = "created_at")
	Date created_at;
	
	@Column(name = "updated_at")
	Date updated_at;
	
	@Column(name = "status", length = 50)
	String status;

	
	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getAssociatedOrg() {
		return associatedOrg;
	}

	public void setAssociatedOrg(String associatedOrg) {
		this.associatedOrg = associatedOrg;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public Date getCreated_at() {
		return created_at;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static UserDb mapUserCOToDB(UserCO objUserCO) throws ParseException
	{
		UserDb objUserDb = new UserDb();
		objUserDb.setUsername(objUserCO.getUsername().toLowerCase());
		objUserDb.setPassword(objUserCO.getPassword());
		objUserDb.setEmail(objUserCO.getEmail());
		objUserDb.setRole(objUserCO.getRole());
		objUserDb.setFirst_name(objUserCO.getFirstName());
		objUserDb.setLast_name(objUserCO.getLastName());
		objUserDb.setMobile_no(objUserCO.getMobileNo());
		objUserDb.setStatus(objUserCO.getStatus());
		objUserDb.setUpdated_at(new Date());
		objUserDb.setDesignation(objUserCO.getDesignation());
		if(objUserCO.getCreated_at() != null) {
			objUserDb.setCreated_at(new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(objUserCO.getCreated_at()));
		}
		
		if(objUserCO.getUserId() != null )
		{
			objUserDb.setUserId(Integer.parseInt(objUserCO.getUserId()));
		}
		return objUserDb;
	}
	
	public static UserCO mapUserDBToCO(UserDb objUserDb) throws ParseException
	{
		UserCO objUserCO = new UserCO();
		objUserCO.setUsername(objUserDb.getUsername());
		//objUserCO.setEmail(objUserDb.getEmail());
		//objUserCO.setRole(objUserDb.getRole());
		objUserCO.setFirstName(objUserDb.getFirst_name());
		objUserCO.setLastName(objUserDb.getLast_name());
		//objUserCO.setMobileNo(objUserDb.getMobile_no());
		objUserCO.setStatus(objUserDb.getStatus());
		//objUserCO.setUserId(Integer.toString(objUserDb.getUserId()));
		//objUserCO.setUpdated_at(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(objUserDb.getUpdated_at()));
		
		if(objUserDb.getCreated_at() != null) {
			//objUserCO.setCreated_at(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(objUserDb.getCreated_at()));
		}
		objUserCO.setUserId(Integer.toString(objUserDb.getUserId()));
		
		return objUserCO;
	}
	
	public static UserCO mapUserLoginDBToCO(UserDb objUserDb) throws ParseException
	{
		UserCO objUserCO = new UserCO();
		objUserCO.setUsername(objUserDb.getUsername());
		objUserCO.setPassword(objUserDb.getPassword());
		objUserCO.setEmail(objUserDb.getEmail());
		objUserCO.setRole(objUserDb.getRole());
		objUserCO.setFirstName(objUserDb.getFirst_name());
		objUserCO.setLastName(objUserDb.getLast_name());
		objUserCO.setMobileNo(objUserDb.getMobile_no());
		objUserCO.setStatus(objUserDb.getStatus());
		objUserCO.setUserId(Integer.toString(objUserDb.getUserId()));
		objUserCO.setUpdated_at(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(objUserDb.getUpdated_at()));
		objUserCO.setDesignation(objUserDb.getDesignation());
		if(objUserDb.getCreated_at() != null) {
			objUserCO.setCreated_at(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(objUserDb.getCreated_at()));
		}
		objUserCO.setUserId(Integer.toString(objUserDb.getUserId()));
		
		return objUserCO;
	}
	
	public static UserCO mapUserUpdateDBToCO(UserDb objUserDb) throws ParseException
	{
		UserCO objUserCO = new UserCO();
		objUserCO.setUsername(objUserDb.getUsername());
		objUserCO.setEmail(objUserDb.getEmail());
		objUserCO.setRole(objUserDb.getRole());
		objUserCO.setFirstName(objUserDb.getFirst_name());
		objUserCO.setLastName(objUserDb.getLast_name());
		objUserCO.setMobileNo(objUserDb.getMobile_no());
		objUserCO.setStatus(objUserDb.getStatus());
		objUserCO.setUserId(Integer.toString(objUserDb.getUserId()));
		objUserCO.setUpdated_at(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(objUserDb.getUpdated_at()));
		objUserCO.setDesignation(objUserDb.getDesignation());
		if(objUserDb.getCreated_at() != null) {
			objUserCO.setCreated_at(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(objUserDb.getCreated_at()));
		}
		objUserCO.setUserId(Integer.toString(objUserDb.getUserId()));
		
		return objUserCO;
	}
	public static UserDb mapUserUpdateCOToDB(UserCO objUserCO) throws ParseException
	{
		UserDb objUserDb = new UserDb();
		objUserDb.setEmail(objUserCO.getEmail());
		objUserDb.setMobile_no(objUserCO.getMobileNo());
		objUserDb.setStatus(objUserCO.getStatus());
		objUserDb.setUpdated_at(new Date());
		objUserDb.setDesignation(objUserCO.getDesignation());
		if(objUserCO.getUserId() != null )
		{
			objUserDb.setUserId(Integer.parseInt(objUserCO.getUserId()));
		}
		return objUserDb;
	}

}
